Source: libfortran-format-perl
Section: perl
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.0
Homepage: https://metacpan.org/release/Fortran-Format
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libfortran-format-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libfortran-format-perl.git
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no

Package: libfortran-format-perl
Architecture: all
Depends: ${misc:Depends}
 , ${perl:Depends}
Multi-Arch: foreign
Description: Package to parse Fortran formats string descriptors in Perl
 This is a Perl implementation of the Fortran 77 formatted input/output
 facility. One possible use is for producing input files for old Fortran
 programs, making sure that their column-oriented records are rigorously
 correct. Fortran formats may also have some advantages over printf in some
 cases: it is very easy to output an array, reusing the format as needed; and
 the syntax for repeated columns is more concise. Unlike printf, for good or
 ill, Fortran-formatted fields never exceed their desired width.
 .
 This implementation was written in pure Perl, with portability and
 correctness in mind. It implements the full ANSI standard for Fortran 77
 Formats (or at least it should). It was not written with speed in mind, so if
 you need to process millions of records it may not be what you need.
